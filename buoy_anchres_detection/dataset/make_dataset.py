# -*- coding: utf-8 -*-
# import project config.py
"""
   Module to prepare the raw data files
"""
import logging
import multiprocessing as mp
import os
import pandas as pd
import shutil
import sys
import zipfile
from dotenv import find_dotenv, load_dotenv
from itertools import repeat
from pathlib import Path
from pathlib import Path
from wget import download as wgd

import buoy_anchres_detection.config as cfg

"""
  Data preparation needs several steps from the offical DOI files in seanoe.org to the usesable data's for training XGBoost
    1 - Download C-RAID delivery from the officials DOI URL
    2 - Unzip each delivery file
    3 - Recursivelly unzip all files in each delevery file
          To save space zipped files are deleted once unzipped
"""


def download_deliveries(delivery_path):
    """
        Download C-RAID data from DOI 10.17882/77184 (https://doi.org/10.17882/77184)
            The download url is configured in config.py  DOI_URL ans DOI_FILES
        Parameters:
            delivery_path (str): Path to save downloaded files
    """
    logger = logging.getLogger(__name__)
    for delivery_file_name in cfg.DOI_FILES:
        dest_path = os.path.join( delivery_path,delivery_file_name)
        url_path = cfg.DOI_URL + delivery_file_name
        if os.path.exists( dest_path ):
            logger.info('Downloading not need for {}'.format(url_path))
        else:
            logger.info('Downloading... {}'.format( url_path ))
            wgd(url_path,  dest_path)

def find_all_zip(path):
    """create array of zipped files"""
    my_files = []
    for root, dirs, files in os.walk(path):
        for i in files:
            if i.endswith(".zip"):
                my_files.append(os.path.join(root, i))
    return my_files

def unzip_deliveries(delivery_path,unzipped_delivery_path):
    zip_files = find_all_zip(delivery_path)

    df = pd.DataFrame(zip_files)
    df['dest'] = unzipped_delivery_path

    parallel_unzip_all(zip_files,df['dest'])


def parallel_unzip_all(zip_files, unzip_files_path):
    """create pool to extract all zip files"""
    pool = mp.Pool(min(mp.cpu_count(), len(zip_files)))  # number of workers
    pool.starmap(unzip, zip(zip_files, unzip_files_path))
    pool.close()


def unzip(zip_file,dest_path):
    """unzips one file"""
    print('extracting {} to {} ...'.format(os.path.basename(zip_file),dest_path))
    zipfile.ZipFile(zip_file).extractall(dest_path)

    #Remove unzipped file
    os.remove(zip_file)


def recursively_unzip_all(path):
    """create pool to extract all zip files"""
    zip_files = find_all_zip(path)
    #stop if no file to unzip
    if zip_files == []:
        return

    df = pd.DataFrame(zip_files)
    df['dest'] = df[0].apply(lambda row : os.path.dirname(row) )

    parallel_unzip_all(zip_files,df['dest'])

    #call the same function to unzip new find files
    recursively_unzip_all(path, remove=True)

    
def make_dataset():
    """ Download and unzip all raw data files.
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')

    download_deliveries(delivery_path)
    unzip_deliveries(delivery_path,unzipped_delivery_path)

    recursively_unzip_all(unzipped_delivery_path)


if __name__ == '__main__':
    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    opts = sys.argv[1:]

    raw_dir = os.path.join(cfg.DATA_DIR,'raw')
    delivery_path= os.path.join(raw_dir,'delivery')
    unzipped_delivery_path=os.path.join(raw_dir,'unzipped_delivery')



    if "--clear_delivery" in opts:
        shutil.rmtree(delivery_path)
        print('directory {} delete'.format(delivery_path))
    if "--clear_unzip" in opts:
        shutil.rmtree(unzipped_delivery_path)
        print('directory {} delete'.format(unzipped_delivery_path))

    Path(raw_dir).mkdir(mode=0o777, parents=True, exist_ok=True)
    Path(delivery_path).mkdir(mode=0o777, parents=True, exist_ok=True)
    Path(unzipped_delivery_path).mkdir(mode=0o777, parents=True, exist_ok=True)


    make_dataset()
