# -*- coding: utf-8 -*-
# import project config.py
"""
   Module to prepare the dataset
"""
import logging
import os
from wget import download as wgd
from pathlib import Path
from dotenv import find_dotenv, load_dotenv
import buoy_anchres_detection.config as cfg
import zipfile
from pathlib import Path
import glob

from itertools import repeat
import multiprocessing as mp
import pandas as pd

import xarray as xr
import numpy as np


import psutil
import time
import multiprocessing as mp
import pyarrow.parquet as pq
import math
from scipy.stats import kurtosis

from geopy import distance

from buoy_anchres_detection.data_utils import mount_nextcloud

def get_DROGUE_STATUS(row,DROGUE_OFF_DATE):
    """get_DROGUE_STATUS base on DATE_TIME and DROGUE_OFF_DATE
         possibles values are True, False, UNKNOW
    """
    if (row['DATE_TIME'] < DROGUE_OFF_DATE):
        return "True"
    elif row['DATE_TIME'] >= DROGUE_OFF_DATE:
        return "False"


def buoy_data_to_dataframe(netcdf_dir):
    input_file = netcdf_dir[0]

    buoy_num = int(os.path.basename(os.path.dirname(input_file)))

    output_file = os.path.join(cfg.DATA_DIR, 'prq', 'buoy' + str(buoy_num) + '.prq')

    if os.path.exists(output_file):
        print('file {} / {} : already done {}'.format(netcdf_dir[1], netcdf_dir[2], input_file))
        return

    print('file {} / {} : {}'. format(netcdf_dir[1],netcdf_dir[2],input_file))
    df = concat(input_file, buoy_num)

    df = feature_extraction(df)
    #print(df)
    #df.to_csv(os.path.join(cfg.DATA_DIR,'buoy_num_'+buoy_num+'.csv'))
    df.to_parquet(output_file, compression='gzip')


def pd_wrapper_buoy_data_to_dataframe():
    '''
        parallelizing the buoy_data_to_dataframe
    '''
    
    files = get_netcdf_files(os.path.join(cfg.DATA_DIR, 'raw'))
    print('files:{}'.format(len(files)))
    nb = range(len(files))

    nb1 = np.full(len(files), len(files))
    lfiles = list(map(lambda a, b, c : [a , b, c], files, nb,nb1))



    # Create the pool
    pool = mp.Pool(min(mp.cpu_count(), len(files))) # number of workers

    # Start processes in the pool
    start = time.time()
    pool.map(buoy_data_to_dataframe, lfiles,7)
    end = time.time()
    print('Completed {} files in: {} sec'.format(len(files), round(end - start)))

    
def concat(input_file,buoy_num):
    '''
       For each buoy concatenate useful data from netcdf and txt files into a single dataframe
    '''
    #print(input_file)

    input_dir = os.path.dirname(input_file)

    ds = xr.open_dataset(input_file,engine='netcdf4')
    #print(list(ds.keys()))

    df1 = pd.DataFrame()

    #extract data from netcdf values
    for h in ['JULD', 'LONGITUDE', 'LATITUDE','SST', 'SST_QC', 'DROGUE', 'DROGUE_QC', 'POSITION_QC']:
        if h not in list(ds.keys()):
            df1[h] = pd.Series([ 0 for x in range(len(df1.index))])
            continue

        if ds[h].ndim == 1:
            df1[h] = pd.Series(ds[h].values.tolist())
        elif ds[h].ndim == 2:
            df1[h] =   pd.Series( [ x[0]  for x in ds[h].values] )
        else:
            raise Exception('ERROR extract netcdf file ' + input_file)

    df1['ID'] = pd.Series([ buoy_num for x in range(len(df1.index))],dtype='int')

    df1['DATE_TIME'] = pd.to_datetime( df1['JULD'] ).dt.round("s") #.astype('datetime64[s]')

    try:
        LAUNCH_DATE = pd.to_datetime(ds['LAUNCH_DATE'].values, format="b'%Y%m%d%H%M%S'")
        END_MISSION_DATE = pd.to_datetime(ds['END_MISSION_DATE'].values,format="b'%Y%m%d%H%M%S'")
    except ValueError:
        LAUNCH_DATE = pd.NaT.now()
        END_MISSION_DATE = pd.NaT.now()

    #filtering invalid data
    df1 = df1.loc[( df1['SST_QC'] <= 2 ) &
                  ( df1['DROGUE_QC'] <= 2 ) &
                  ( df1['DATE_TIME'] >=  LAUNCH_DATE ) &
                  ( df1['DATE_TIME'] <=  END_MISSION_DATE )]

    df1 = df1.dropna()
    if len(df1) == 0: return

    # Avoiding errors (drogue off date missing) and adding presence flag (TRUE/FALSE)
    DROGUE_OFF_DATE = str(ds['DROGUE_OFF_DATE'].values)
    if  DROGUE_OFF_DATE != "b'              '":
        DROGUE_OFF_DATE = pd.to_datetime(DROGUE_OFF_DATE, format="b'%Y%m%d%H%M%S'")#.astype('datetime64[s]')
        df1['DROGUE_STATUS']= df1.apply( lambda row : get_DROGUE_STATUS(row,DROGUE_OFF_DATE), axis=1 )
    else:
        df1['DROGUE_STATUS'] = pd.Series([ 'UNKNOW' for x in range(len(df1.index))],dtype='str')


    try:
        #read _ewss_nsss_era5 file
        FILE_EW = os.path.join(input_dir, str(buoy_num) + '_complementary_data'  ,str(buoy_num) + '_ewss_nsss_era5_extracted.txt')

        ew_df = pd.read_csv(FILE_EW,
                           delim_whitespace=True,
                           header=None,
                           converters={0:int,1:str,2:str,3:str,4:str,5:str,6:str,
                                       7:float,8:float,9:float,10:float,11:float,12:float,13:float,
                                       14:float,15:float,16:float,17:float,18:float,19:float,20:float,
                                       21:float,22:float,23:float,24:float,25:float,26:float}
                            )
    except FileNotFoundError:
        print("ERROR : file not found {}".format(FILE_EW))
        return


    try:
        ew_df['EW_DATE']  = pd.to_datetime(ew_df[1]+ew_df[2]+ew_df[3]+ew_df[4]+ew_df[5]+ew_df[6],format='%Y%m%d%H%M%S')
    except ValueError:
        print("ERREUR : date format error in file {} ".format(FILE_EW))
        return

    ew_df = ew_df.rename(columns={ew_df.columns[9]: 'EWSS', ew_df.columns[10]: 'NSSS' })
    ew_df = ew_df[['EW_DATE','EWSS','NSSS']]
    ew_df = ew_df.set_index('EW_DATE')


    df1 = pd.merge_asof(df1, ew_df, left_on='DATE_TIME',right_on='EW_DATE',  tolerance=pd.Timedelta("3s"), direction='nearest')

    #read _sst_msl_era5 file
    try:
        FILE_SST = os.path.join(input_dir, str(buoy_num) + '_complementary_data'  , str(buoy_num) + '_sst_msl_era5_extracted.txt')
        sst_df = pd.read_csv(FILE_SST,
                           delim_whitespace=True,
                           header=None,
                           converters={0:int,1:str,2:str,3:str,4:str,5:str,6:str,
                                       7:float,8:float,9:float,10:float,11:float,12:float,13:float,
                                       14:float,15:float,16:float,17:float,18:float,19:float,20:float,
                                       21:float,22:float,23:float,24:float,25:float,26:float} )
    except FileNotFoundError:
        print("ERROR : file not found ".format(FILE_SST))
        return


    try:
        sst_df['SST_DATE'] = pd.to_datetime(sst_df[1]+sst_df[2]+sst_df[3]+sst_df[4]+sst_df[5]+sst_df[6],format='%Y%m%d%H%M%S')
    except ValueError:
        print("ERREUR : date format error in file {} ".format(FILE_SST))
        return

    sst_df = sst_df.rename(columns={sst_df.columns[9]: 'SST_ERA' })
    sst_df = sst_df[['SST_DATE','SST_ERA']]
    sst_df = sst_df.set_index('SST_DATE')

    df1 = pd.merge_asof(df1, sst_df, left_on='DATE_TIME', right_on='SST_DATE', tolerance=pd.Timedelta("3s"), direction='nearest')


    #read _u10_v10_era5 file
    try:
        FILE_WIND = os.path.join(input_dir, str(buoy_num) + '_complementary_data'  ,str(buoy_num) + '_u10_v10_era5_extracted.txt')
        wind_df = pd.read_csv(FILE_WIND,
                           delim_whitespace=True,
                           header=None,
                           converters={0:int,1:str,2:str,3:str,4:str,5:str,6:str,
                                       7:float,8:float,9:float,10:float,11:float,12:float,13:float,
                                       14:float,15:float,16:float,17:float,18:float,19:float,20:float,
                                       21:float,22:float,23:float,24:float,25:float,26:float}
                              )
    except FileNotFoundError:
        print("ERROR : file not found ".format(FILE_WIND))
        return

    try:
        wind_df['WIND_DATE'] = pd.to_datetime(wind_df[1]+wind_df[2]+wind_df[3]+wind_df[4]+wind_df[5]+wind_df[6], format='%Y%m%d%H%M%S')
    except ValueError:
        print("ERREUR : date format error in file {} ".format(FILE_WIND))
        return



    wind_df = wind_df.rename(columns={wind_df.columns[9]: 'U10' , wind_df.columns[10]: 'V' })
    wind_df = wind_df[['WIND_DATE','U10','V']]
    wind_df = wind_df.set_index('WIND_DATE')

    df1 = pd.merge_asof(df1, wind_df, left_on='DATE_TIME', right_on='WIND_DATE', tolerance=pd.Timedelta("3s"), direction='nearest')
    df1 = df1.dropna()

    if len(df1) == 0 : return

    df1['DIFFTIME'] = (df1['DATE_TIME'].diff(+1).astype('timedelta64[s]')).div(3600).round(3).fillna(0)
    df1['WIND_SPEED'] = ( df1['U10']**2 + df1['V']**2 )**(0.5) # racine.carre(U10² + v²)
    df1['WIND_DIR'] = ( np.arctan2( df1['U10'] / df1['WIND_SPEED'] , df1['V'] / df1['WIND_SPEED'] ) * 180 / np.pi ) + 180


    return df1


def feature_extraction(df):
    '''
        calculate advenced data based on buoy and era5 data
    '''

    df = df.sort_values(by= 'DATE_TIME')
    #print(df)

    #shift LATITUDE and LONGITUDE
    df['LATITUDE1'] = df['LATITUDE'].shift(+1)
    df['LONGITUDE1'] = df['LONGITUDE'].shift(+1)
    df.loc[0,'LATITUDE1'] = df.loc[0,'LATITUDE']
    df.loc[0,'LONGITUDE1'] = df.loc[0,'LONGITUDE']
    df['LATITUDE-1'] = df['LATITUDE'].shift(-1)
    df['LONGITUDE-1'] = df['LONGITUDE'].shift(-1)



    df['DIST_FROM_PREV_MILE_KM'] = df.apply(  lambda row : 
        distance.great_circle( (row['LATITUDE'] , row['LONGITUDE']), (row['LATITUDE1'] , row['LONGITUDE1'])   ).km , axis= 1)
        # .diff().astype('timedelta64[m]')  #.round(3).fillna(0)

    df['DIST_FROM_PREV_MILE'] = df['DIST_FROM_PREV_MILE_KM'].div(1.852)
    df['MEAN_SPEED'] = df['DIST_FROM_PREV_MILE'].div(df['DIFFTIME']).fillna(0)



    df['DIST_SUM'] = df['DIST_FROM_PREV_MILE'].cumsum()

    # Acceleration in m.sec#
    df['TIME_DELTA_S'] = df['DATE_TIME'].diff(-1).astype('timedelta64[s]').fillna(0)

    df['ACCELERATION'] = df['MEAN_SPEED'].diff(-1).fillna(0).mul( 1852/3600/ df['TIME_DELTA_S'] )
    df['JERK'] = (df['ACCELERATION'].diff(-1).fillna(0)).div(  df['TIME_DELTA_S'] )

    df['BEARING'] = df.apply( lambda row : calcBearing( row['LONGITUDE'], row['LATITUDE'], row['LONGITUDE-1'] , row['LATITUDE-1']  ), axis= 1 )

    df['BEARING_RATE'] = df['BEARING'].diff(-1).abs()

    df['ELAPSED_TIME'] = df['DIFFTIME'].div(24).cumsum()


    df['ABS_BEARING'] = df.apply( lambda  row :  ( 180 - abs(row['BEARING'])  ) + 180 if row['BEARING'] <0 else row['BEARING'], axis= 1 )
    df['SPEED_DIFF_NORM'] = df.apply( lambda  row :  ( row['MEAN_SPEED'] - row['WIND_SPEED'] ) / ( row['MEAN_SPEED'] + row['WIND_SPEED'] ) , axis= 1 )
    df['ABS_SPEED_DIFF'] = df.apply( lambda  row :  abs(row['MEAN_SPEED'] - row['WIND_SPEED']   ) , axis= 1 )
    df['BEARING_DIFF_NORM'] = df.apply( lambda  row :  (  row['ABS_BEARING'] - row['WIND_DIR'] ) / ( row['ABS_BEARING'] + row['WIND_DIR'] )     , axis= 1 )
    df['ABS_BEARING_DIFF'] = df.apply( lambda  row : abs(row['ABS_BEARING'] - row['WIND_DIR']   ) , axis= 1 )
    df['RELAT_BEARING_DIFF'] = df.apply( lambda  row : (  row['ABS_BEARING'] - row['WIND_DIR'] ) / row['WIND_DIR']  , axis= 1 )
    df['RELAT_SPEED_DIFF'] = df.apply( lambda  row : ( row['WIND_SPEED'] - row['MEAN_SPEED'] ) / row['WIND_SPEED']  , axis= 1 )


    #TODO : in POC the STRAIGHTNESS is based on trajr R package.
    #need a study for doing the same in python. Maybe using the movingpandas library
    df['STRAIGHTNESS'] = 0
 
    # TODO : in POC the folowing columns are filtered with 0,05 and 0,95 percentile excluded
    # MEAN_SPEED
    # ACCELERATION
    # JERK
    # BEARING_RATE
    # ABS_SPEED_DIFF
    # SPEED_DIFF_NORM
    # DIR_CHANGE
    # BEARING_DIFF
    # ABS_BEARING_DIF
    # BEARING_DIFF_NORM
    # EWSS
    # NSSS
    # U10
    # V
    # DIST_FROM_PREV

    df['ABS_SST_DIFF'] = (df['SST'] - df['SST_ERA']).abs()
    df['SST_DIFF'] = (df['SST_ERA'] - df['SST'])

    df['DROGUE_LAG1']=df['DROGUE'].shift(+1)
    df['DROGUE_LAG2']=df['DROGUE'].shift(+2)
    df['DROGUE_SHIFT1']=df['DROGUE'].shift(-1)
    df['DROGUE_SHIFT2']=df['DROGUE'].shift(-2)

    def dispersion_extract(df=None, var=None):
        import warnings
        warnings.filterwarnings('ignore')
        df_selection = df[var].expanding(0, axis=0)
        df[var+'_PREV_RANGE']= df_selection.quantile(0.75) -  df_selection.quantile(0.25)
        df[var+'_PREV_MED'] = df_selection.median()
        df[var+'_PREV_AVE'] = df_selection.mean()
        df[var+'_AVE_DELTA'] = 0
        df[var+'_NEXT_RANGE'] = 0
        df[var+'_NEXT_MED'] = 0
        df[var+'_NEXT_AVE'] = 0
        df[var+'_RANGE_DELTA'] = 0
        df[var+'_PREV_VAR'] = df_selection.var()
        df[var+'_NEXT_VAR'] = 0
        df[var+'_VAR_DELTA'] = 0
        df[var+'_PREV_MIDRANGE'] = df_selection.max() - df_selection.min()
        df[var+'_NEXT_MIDRANGE'] = 0
        df[var+'_PREV_CV'] =  df_selection.std() / df_selection.mean()
        df[var+'_NEXT_CV'] = 0
        df[var+'_PREV_KURTOSIS'] = df_selection.apply( lambda  row : kurtosis( row, fisher=True ))
        df[var+'_NEXT_KURTOSIS'] = 0
        df[var+'_MIDRANGE_DELTA'] = 0
        df[var+'_PREV_SD'] = df_selection.std()
        df[var+'_NEXT_SD'] = 0
        df[var+'_SD_DELTA'] = 0

        return df


    df = dispersion_extract(df, 'SPEED_DIFF_NORM')
    df = dispersion_extract(df, 'DROGUE')
    df = dispersion_extract(df, 'SST')
    df = dispersion_extract(df, 'DIFFTIME')





    df["SPEED_RATE"] = df.apply( lambda  row : row['MEAN_SPEED'] / row['WIND_SPEED']  , axis= 1 )

    #print( df[['DATE_TIME','LATITUDE','LONGITUDE',
    #'LATITUDE1','LONGITUDE1',
    # 'DIST_FROM_PREV_MILE','DELTA','DIFFTIME','MEAN_SPEED','DIST_SUM','DIST_FROM_PREV_MILE','TIME_DELTA_S', 'ACCELERATION',
    #               'JERK','BEARING','BEARING_RATE',
    #                'ELAPSED_TIME','ABS_BEARING',
    #           'SPEED_DIFF_NORM',
    # 'ABS_SPEED_DIFF',
    #                'BEARING_DIFF_NORM','ABS_BEARING_DIFF',
    #               'RELAT_BEARING_DIFF','RELAT_SPEED_DIFF',
    #   'STRAIGHTNESS',
    #               'SPEED_RATE',
    #          'ABS_SST_DIFF',
    #         'SST_DIFF',
    #         'DROGUE','DROGUE_LAG1','DROGUE_LAG2','DROGUE_SHIFT1','DROGUE_SHIFT2',
    #          'SPEED_DIFF_NORM_PREV_RANGE',
    #          'SPEED_DIFF_NORM_PREV_KURTOSIS'
    #           ]].tail(50) )

    # print( df)

    return df

def calcBearing (lat1, long1, lat2, long2):
    '''
     Equivalent of TrackReconstruction.CalcBearing R function
       https://rdrr.io/cran/TrackReconstruction/man/CalcBearing.html
    '''
    initialLat = lat1 / 360 * 2 * np.pi
    initialLong = long1 / 360 * 2 * np.pi
    finalLat = lat2 / 360 * 2 * np.pi
    finalLong = long2 / 360 * 2 * np.pi
    bearing = math.atan2(math.sin(finalLong - initialLong) * math.cos(finalLat),
          math.cos(initialLat) * math.sin(finalLat) - math.sin(initialLat) * math.cos(finalLat) * math.cos(finalLong - initialLong))
    return math.degrees(bearing)


def get_netcdf_files(path):
    '''
    Get the files of a directory
    '''

    my_files = []
    for root, dirs, files in os.walk(path):
        for i in files:
            if ( i.endswith(".nc") and ( root.find('data_no_drifter') == -1 )) :
                #i.startswith("") and   and (i.startswith('1200482'))
                my_files.append(os.path.join(root, i))

    return my_files



def parquet_append(filepath:Path or str, df: pd.DataFrame) -> None:
    '''
     Append to dataframe to existing .parquet file. Reads original .parquet file in, appends new dataframe, writes new .parquet file out.
    :param filepath: Filepath for parquet file.
    :param df: Pandas dataframe to append. Must be same schema as original.
    '''

    import pyarrow as pa
    import pyarrow.parquet as pq
    
    if not os.path.exists(filepath):
        df.to_parquet(filepath, compression='gzip')
        return

    table_original_file = pq.read_table(source=filepath,  pre_buffer=False, use_threads=True, memory_map=True)  # Use memory map for speed.
    table_to_append = pa.Table.from_pandas(df)
    table_to_append = table_to_append.cast(table_original_file.schema)  # Attempt to cast new schema to existing, e.g. datetime64[ns] to datetime64[us] (may throw otherwise).
    handle = pq.ParquetWriter(filepath, table_original_file.schema)  # Overwrite old file with empty.
    handle.write_table(table_original_file)
    handle.write_table(table_to_append)
    handle.close()  # Writes binary footer. Until this occurs, .parquet file is not usable.


def concat_prq_files(prq_dir,prq_output_file):
    '''
        Concat all individual parquet files in one parquet file 
    '''

    if os.path.exists(prq_output_file):
        os.remove(prq_output_file)

    files = glob.glob(os.path.join(cfg.DATA_DIR, 'prq','*.prq'))

    i = 0
    i_max = 150
    df_t = pd.DataFrame()
    for f in files:
        i = i+1
        print(' {} : {} '.format(i,f))

        df_f = pd.read_parquet(f)
        df_t = pd.concat([df_t,df_f])

        if i % i_max == 0 or i == len(files):
            print(' {} : append '.format(i))
            parquet_append(prq_output_file,df_t)
            df_t = pd.DataFrame()


def build_features():
    """ Runs data processing scripts to turn raw data from (../raw) into
        cleaned data ready to be analyzed saved in parquet file.
    """
    logger = logging.getLogger(__name__)
    logger.info('making final data set from raw data')


    raw_dir = os.path.join(cfg.DATA_DIR,'raw')
    prq_dir = os.path.join(cfg.DATA_DIR, 'prq')

    Path(prq_dir).mkdir(mode=0o777, parents=True, exist_ok=True)


    pd_wrapper_buoy_data_to_dataframe()
    
    


    #TODO don't concat un one big file that can't be loaded in memory
    #    prq_output_file = os.path.join(cfg.LEARNING_DIR,'buyo_full.prq')
    #    concat_prq_files(prq_dir,prq_output_file)


    #updload generated full parquet file to nextcloud
    project_dir = Path(__file__).resolve().parents[2]

    logger.info('uploading to nextcloud')    
    mount_nextcloud( os.path.basename(prq_output_file), cfg.NEXTCLOUD_DATA_DIR )

                    
                    
if __name__ == '__main__':

    log_fmt = '%(asctime)s - %(name)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=logging.INFO, format=log_fmt)

    # not used in this stub but often useful for finding various files
    project_dir = Path(__file__).resolve().parents[2]

    prq_dir = os.path.join(cfg.DATA_DIR, 'prq')

    Path(prq_dir).mkdir(mode=0o777, parents=True, exist_ok=True)


    # find .env automagically by walking up directories until it's found, then
    # load up the .env entries as environment variables
    load_dotenv(find_dotenv())

    build_features()


