buoy-anchres-detection
==============================

[![Build Status](https://jenkins.indigo-datacloud.eu/buildStatus/icon?job=Pipeline-as-code/DEEP-OC-org/buoy_anchres_detection/master)](https://jenkins.indigo-datacloud.eu/job/Pipeline-as-code/job/DEEP-OC-org/job/buoy_anchres_detection/job/master)

Identification of the loss of floating anchors on drifting buoys based on GPS positions and other data transmitted by the drifting buoys. Part of C-RAID Project (Copernicus Reprocessing of Argos and Iridium Drifters)

Project Organization
------------

    ├── LICENSE
    ├── README.md              <- The top-level README for developers using this project.
    ├── data
    │   └── raw                <- The original, immutable data dump.
    │
    ├── docs                   <- A default Sphinx project; see sphinx-doc.org for details
    │
    ├── models                 <- Trained and serialized models, model predictions, or model summaries
    │
    ├── notebooks              <- Jupyter notebooks. Naming convention is a number (for ordering),
    │                             the creator's initials (if many user development), 
    │                             and a short `_` delimited description, e.g.
    │                             `1.0-jqp-initial_data_exploration.ipynb`.
    │
    ├── references             <- Data dictionaries, manuals, and all other explanatory materials.
    │
    ├── reports                <- Generated analysis as HTML, PDF, LaTeX, etc.
    │   └── figures            <- Generated graphics and figures to be used in reporting
    │
    ├── requirements.txt       <- The requirements file for reproducing the analysis environment, e.g.
    │                             generated with `pip freeze > requirements.txt`
    ├── test-requirements.txt  <- The requirements file for the test environment
    │
    ├── setup.py               <- makes project pip installable (pip install -e .) so buoy_anchres_detection can be imported
    ├── buoy_anchres_detection    <- Source code for use in this project.
    │   ├── __init__.py        <- Makes buoy_anchres_detection a Python module
    │   │
    │   ├── dataset            <- Scripts to download or generate data
    │   │   └── make_dataset.py
    │   │
    │   ├── features           <- Scripts to turn raw data into features for modeling
    │   │   └── build_features.py
    │   │
    │   ├── models             <- Scripts to train models and make predictions
    │   │   └── deep_api.py    <- Main script for the integration with DEEP API
    │   │
    │   └── tests              <- Scripts to perfrom code testing
    │   │
    │   └── visualization      <- Scripts to create exploratory and results oriented visualizations
    │       └── visualize.py
    │
    └── tox.ini                <- tox file with settings for running tox; see tox.testrun.org


--------

<p><small>Project based on the <a target="_blank" href="https://drivendata.github.io/cookiecutter-data-science/">cookiecutter data science project template</a>. #cookiecutterdatascience</small></p>



# Dataset preparation

## Configuration

The URL to download C-RAID delivery files are setup in the DOI_URL and DOI_FILE parameter's of the /srv/buoy_anchres_detection/buoy_anchres_detection/config.py file

## Download and unzip files


```bash
     python /srv/buoy_anchres_detection/buoy_anchres_detection/dataset/make_dataset.py
     
     make_dataset.py [--clear_delivery] [--clear_unzip]
```


## build_features : Concat and Extract data


```bash
     python /srv/buoy_anchres_detection/buoy_anchres_detection/features/build_features.py
     
```


